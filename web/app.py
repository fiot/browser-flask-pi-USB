from flask import Flask, render_template
from redis import Redis, RedisError
import os
import socket
import cindymod
import time
from subprocess import call
import json

counter = 0

# Connect to Redis
redis = Redis(host="redis", db=0, socket_connect_timeout=2, socket_timeout=2)

app = Flask(__name__)

@app.route("/")
def hello():
    geocode=[3,2]
    return render_template("main.html",geocode=geocode)

@app.route('/connect')
def connect():
    try:
        port="/dev/ttyACM0"
        print("Connected to",port)
        peep=cindymod.Peep(port)
        peep.flush()
        print "worked!" 
    except:
        print "oops!"
    return "Hello world!"

@app.route('/capture_spectrum')
def capture_spectrum():
    try:
        port="/dev/ttyACM0"
        print("Connected to",port)
        peep=cindymod.Peep(port)
        peep.flush()
        print(counter)
        sdata=peep.read()
        print(sdata)
        print (json.dump(sdata))
        time.sleep(1)
        peep.close()
        #sdata=peep.light_on()
        #print(sdata)
        #time.sleep(3)
        #peep.flush()
        #sdata=peep.light_off()
        #user = {'firstname': "Mr.", 'lastname': "My Father's Son"}
        #return render_template("main.html", user=user)
        geocode=[6,7]
        return render_template("main.html",geocode=geocode)        
    except:
	print "oops!"
    return "Hello world!"

@app.route('/background_process_test')
def background_process_test():
    try:
        #call(["omxplayer","/home/pi/gitwork/dockerized-flask-app/web/static/peepikiki.mp3"]) 
        port="/dev/ttyACM0"
        print("Connected to",port)
        peep=cindymod.Peep(port)
        orig=90
        
        endpoint = 135
 
        # reset position
        peep.flush()
        time.sleep(1)
        peep.servo_place(orig)
        time.sleep(2)

        peep.servo_place(endpoint) 
        time.sleep(2)
        for i in range(0,3): 
            peep.servo_place(endpoint-10)
            peep.flush()
            time.sleep(.1)
            peep.servo_place(endpoint+10)
            peep.flush()
            time.sleep(.5)

        peep.servo_place(orig)
    except RedisError:
        print("oops")

    return 'Hello, World!'
    
@app.route("/cindy")
def cindy():
    try:
        print("hello")
        port="/dev/ttyACM0"
        print("Connected to",port)
        peep=cindymod.Peep(port)
        orig=90
	endpoint = 135
        peep.flush()
        peep.flush()
        time.sleep(1)
        peep.servo_place(orig)
        time.sleep(2)
    
        peep.servo_place(endpoint)
   
        for i in range(0,3): 
            peep.servo_place(endpoint-10)
            peep.flush()
            time.sleep(.1)
            peep.servo_place(endpoint+10)
            peep.flush()
            time.sleep(.5)

        peep.servo_place(orig)

    except RedisError:
        print("oops")

    return 'Hello, World!'


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)

