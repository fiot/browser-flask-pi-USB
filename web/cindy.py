import os, glob, serial,time

class Peep(object):
    def __init__(self, port):
        self._ser = serial.Serial(port,baudrate=9600)
    def set_integration_time(self, seconds):
        cmd = "SPEC.INTEG %0.6f\n" % seconds
        self._ser.write(cmd.encode('utf8'))
    def read(self):
        self._ser.write(b"SPEC.READ?\n")
        sdata = self._ser.readline()
        sdata = array([int(p) for p in sdata.split(b",")])
        #self._ser.write(b"SPEC.TIMING?\n")
        #tdata = self._ser.readline()
        #tdata = array([int(p) for p in tdata.split(b",")])
        #return (sdata, tdata)
        return sdata
    def light_on(self):
        self._ser.write(b"LIGHT.ON!\n")
    def light_off(self):
        self._ser.write(b"LIGHT.OFF!\n")
    def servo_sweep(self,servo_start,servo_end,stepsize):
        cmd = "M %d %d %d\n" % (servo_start, servo_end, stepsize)
        print(cmd)
        self._ser.write(cmd.encode('utf8'))
    def servo_place(self,servo_pos):
        cmd = "M %d\n" % servo_pos
        print(cmd)
        self._ser.write(cmd.encode('utf8'))
    def flush(self):
        self._ser.flushInput()
        self._ser.flushOutput()

if __name__ == "__main__":
   
    port = glob.glob("/dev/ttyUSB*")[0]
    
    print("Connected to",port)
    
    peep = Peep(port)
    
    orig = 90
    
    # reset position
    peep.flush()
    time.sleep(1)
    peep.servo_place(orig)
    time.sleep(2)
    
    for i in range(0,60):
        peep.flush()
        time.sleep(.05)
        peep.servo_place(orig+i)
        time.sleep(.05)
    
    peep.flush()
    time.sleep(1)
    peep.servo_place(orig)
    time.sleep(2)
